## [0.0.3](https://gitlab.com/mfgames-writing/mfgames-ncx-js/compare/v0.0.2...v0.0.3) (2018-08-11)


### Bug Fixes

* updating package management ([baa59b1](https://gitlab.com/mfgames-writing/mfgames-ncx-js/commit/baa59b1))
