# NCX

`mfgames-ncx` is a library for reading, manipulating, and writing out NCX (Navigation Center eXtended) files used for EPUB and Open Packaging Format files.
