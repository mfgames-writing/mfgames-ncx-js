import * as xmlBuilder from "xmlbuilder";

/**
* Defines the top-level class for representing a NCX file.
*/
export class Ncx
{
    public title: string;
    public uid: string;
    public cover: string;
    public points: NcxPoint[] = [];

    public addPoint(id: string, title: string, href: string, parent: NcxPoint | undefined = undefined): NcxPoint
    {
        let point = new NcxPoint(id, title, href);

        if (parent)
        {
            parent.points.push(point);
        } else
        {
            this.points.push(point);
        }

        return point;
    }

    public toBuffer(): Buffer
    {
        // Write out the prolog for the file.
        let xmlns = "http://www.daisy.org/z3986/2005/ncx/";
        const builder: any = xmlBuilder;
        let xml = builder
            .begin()
            .dec("1.0", "UTF-8")
            .ele("ncx", { version: "2005-1", xmlns: xmlns });

        // Write out the additional elements.
        this.writeHead(xml);
        this.writeDocTitle(xml);
        this.writeNavMap(xml);

        // Finish up the XML and write it out. This is not a string, so we
        // force it into one by appending "".
        xml.end({ pretty: true });
        return new Buffer(xml + "", "utf-8");
    }

    private writeDocTitle(xml: any)
    {
        if (!this.title) return;

        xml = xml.ele("docTitle").ele("text", this.title);
    }

    private writeHead(xml: any)
    {
        xml = xml.ele("head");

        if (this.uid) xml.ele("meta", { name: "dtb:uid", content: this.uid });

        xml.ele("meta", { name: "dtb:depth", content: "1" });
        xml.ele("meta", { name: "dtb:totalPageCount", content: "0" });
        xml.ele("meta", { name: "dtb:maxPageNumber", content: "0" });

        if (this.cover) xml.ele("meta", { name: "cover", content: this.cover });
    }

    private writeNavMap(xml: any)
    {
        // Write out the top-level map.
        xml = xml.ele("navMap");

        // Loop through the point and add each one.
        let order = 1;

        for (let point of this.points)
        {
            order = point.write(xml, order);
        }
    }
}

export class NcxPoint
{
    public id: string;
    public title: string;
    public href: string;
    public points: NcxPoint[] = [];

    constructor(id: string, title: string, href: string)
    {
        this.id = id;
        this.title = title;
        this.href = href;
    }

    public write(xml: any, order: number): number
    {
        // Write out this navigation point.
        let px = xml.ele("navPoint", { id: this.id, playOrder: order++ });

        px.ele("navLabel").ele("text", this.title);
        px.ele("content", { src: this.href });

        // Add in any recursive points, if needed.
        for (let point of this.points)
        {
            order = point.write(px, order);
        }

        return order;
    }
}
